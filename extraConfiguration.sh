#!/bin/bash

# sqlite3
apt update
apt install sqlite3

# common
composer update
php maintenance/update.php

cd /var/www/html/w/extensions/CheckUser
composer install --no-dev

"$@"
